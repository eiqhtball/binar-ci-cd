var hitung = document.getElementById('hitung');

hitung.addEventListener('click', function(evt){
    // console.log('Hello World');
    var input1 = document.getElementById('angka1');
    var input2 = document.getElementById('angka2');

    var angka1 = parseInt(input1.value);
    var angka2 = parseInt(input2.value);
    
    var operasi = document.querySelector('input[name=operasi]:checked').value;    

    if(isNaN(angka1)&&isNaN(angka2)){
            document.querySelector('h1').innerHTML = "Kolom angka 1 & 2 Harus Terisi!";
            document.querySelector('h1').style.color = '#00ff00 ';
        } 

        else if (isNaN(angka1)){
                document.querySelector('h1').innerHTML = "Kolom angka 1 Harus Terisi!";     
            } 

            else if (isNaN(angka2)){
                    document.querySelector('h1').innerHTML = "Kolom angka 2 Harus Terisi!";
                }

                else {
                        var result = 0;
                        switch (operasi) {
                            case 'penjumlahan':
                                result = angka1 + angka2;
                                break;

                            case 'pengurangan':
                                result = angka1 - angka2;
                                break;

                            case 'perkalian':
                                result = angka1 * angka2;
                                break;

                            case 'pembagian':
                                result = angka1 / angka2;
                                break;
                        }
                        document.querySelector('h1').innerHTML = "Hasil Penjumlahan : " + result;
                        console.log("Sedang melakukan", operasi);
                    }  
});